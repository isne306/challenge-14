﻿#include <algorithm>
#include <iostream>
using namespace std;

void quicksort(int array[], int size)
{
	int first = array[0];
	int middle = array[size/2];
	int last = array[size-1];

  	int find[] = { first,middle,last };
	int fsize = sizeof(find)/sizeof(int);
	sort(find, find + fsize);
	int median = find[1];


	int pivot = median;
	int size1 = 0, size2 = 0, sL = 0, sR = 0;
	for (int i = 0; i < size; i++)
	{
		if (array[i] < pivot)
			size1++;
		else if (array[i] > pivot)
			size2++;
	}

	int *sub1 = new int[size1]; 
	int *sub2 = new int[size2]; 

	if (size > 2)
	{
		for (int i = 0; i < size; i++)
		{
			if (array[i] < pivot)
			{
				sub1[sL] = array[i];
				sL++;
			}
			else if (array[i] > pivot)
			{
				sub2[sR] = array[i];
				sR++;
			}
		}
		quicksort(sub1, size1);
		quicksort(sub2, size2);
		for (int j = 0; j < size1; j++)
		{
			array[j] = sub1[j];
		}
		array[size1] = pivot;
		for (int j = 0; j < size2; j++)
		{
			array[size1 + 1 + j] = sub2[j];
		}
	}
	else if (size == 2)
	{
		if (array[0] > array[1])
		{
			swap(array[0], array[1]);
		}
	}
}

void merge(int array[], int sub1[], int sub2[], int size) 
{
	int i = 0, i1 = 0, i2 = 0; 
	int mid1 = size / 2;
	int mid2 = size - mid1;
	while ((i1 < mid1) && (i2 < mid2))
	{
		if (sub1[i1] < sub2[i2])
		{
			array[i] = sub1[i1];
			i++;
			i1++;
		}
		else
		{
			array[i] = sub2[i2];
			i++;
			i2++;
		}
	}
	while (i1 < mid1)
	{
		array[i++] = sub1[i1++];
	}
	while (i2 < mid2)
	{
		array[i++] = sub2[i2++];
	}
}

void mergesort(int a[], int size)
{
	int mid1 = size / 2;
	int mid2 = size - mid1;
	int *al = new int[mid1];
	int *ar = new int[mid2];
	if (size < 2)
	{
		return;
	}
	else
	{
		for (int i = 0; i < mid1; i++)
		{
			al[i] = a[i];
		}
		for (int i = 0; i < mid2; i++)
		{
			ar[i] = a[mid1 + i];
		}
		mergesort(al, mid1);
		mergesort(ar, mid2);
		merge(a, al, ar, size);

	}
}

int main()
{


	cout << "Quick sort" << endl<< endl; 
	int quick[] = {15, 24, 32, 41, 84, 52, 1, 39, 22,65 };

	cout << "Before : ";
	for (int i = 0; i < 9; i++)
	{
		cout << quick[i] << " ";
	}
	cout << endl;
	quicksort(quick, 9);
	cout << "After by Quick sort  : ";
	for (int i = 0; i < 9; i++)
	{
		cout << quick[i] << " ";
	}
	cout << endl;cout << endl;cout << endl;cout << endl;

	cout << "Merge Sort" << endl << endl;
	int m[] = { 16, 27, 34, 17, 5, 97, 48, 12, 87, 9 };
	cout << "Before : ";
	for (int i = 0; i <9; i++)
	{
		cout << m[i] << " ";
	}
	cout << endl;
	mergesort(m, 9);
	cout << "After by Merge Sort  : ";
	for (int i = 0; i <9; i++)
	{
		cout << m[i] << " ";
	}
	cout << endl;

return 0;
}
